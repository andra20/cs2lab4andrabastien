package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.paint.Color;

/**
 * Myriapoda class
 * 
 * @author Andra
 * @version spring 2020
 */
public class Myriapoda extends Bug {
	private int numberSegments;

	/**
	 * new Myriapoda class
	 * 
	 * @precondition length>0&&numberLegs>0&&numberSegments>0&&color!=null
	 * @postcondition getLength() == length && getNumberLegs() == numberLegs &&
	 *                getColor() == color && getNumberSegments() == numberSegemnts
	 * @param length         the length
	 * @param numberLegs     the number of legs
	 * @param numberSegments the number segment
	 * @param color          the color
	 */
	public Myriapoda(double length, int numberLegs, int numberSegments, Color color) {
		super();
		if (numberSegments < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_NUMBER_SEGMENTS);
		}
		this.numberSegments = numberSegments;
	}

	/**
	 * get number segments
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the numberSegments
	 */
	public int getNumberSegments() {
		return this.numberSegments;
	}

	@Override
	public String toString() {
		this.numberSegments = 10;
		double numLegs = this.getNumberLegs();
		numLegs = 60;
		double length = this.getLength();
		length = 140;
		String output = " Myriapoda type of Bug length " + length + " # legs " + numLegs
				+ " color " + this.getColor() + " # segments " + this.numberSegments;
		return output;

	}	
}
