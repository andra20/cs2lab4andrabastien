package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.paint.Color;

/**
 * Bee class
 * 
 * @author Andra
 * @version spring 2020
 */
public class Bee extends Insect {
	private BeeCaste caste;

	/**
	 * instantiate the new Bee class
	 * 
	 * @precondition length>0&&caste!=null
	 * @postcondition getLength() == length && getNumberLegs() == 6 && getColor() ==
	 *                Color.BLACK && isWinged() == true && getCaste() == caste
	 * @param length the length
	 * @param caste  the caste
	 */
	public Bee(double length, BeeCaste caste) {
		super(length, Color.BLACK, true);
		if (caste == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BEECASTE);
		}
		this.caste = caste;

	}

	/**
	 * get caste
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the caste
	 */
	public BeeCaste getCaste() {
		return this.caste;
	}

	/**
	 * set the caste
	 *   
	 * @precondition caste !=null
	 * @postcondition none
	 * @param caste the caste to set
	 */
	public void setCaste(BeeCaste caste) {
		this.caste = caste;
	}

	@Override
	public String toString() {
	    double length = this.getLength();
	    length = 100;
		String output = " Bee type of Insect  " + length + " #legs " + this.getNumberLegs()
				+ " color " + Color.BLACK + " winged " + this.isWinged() + " caste " + BeeCaste.QUEEN;
		return output;
	}
}
