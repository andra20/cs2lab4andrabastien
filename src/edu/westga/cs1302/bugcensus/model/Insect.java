package edu.westga.cs1302.bugcensus.model;

import javafx.scene.paint.Color;

/**
 * the Insect class
 * 
 * @author Andra
 * @version spring 2020
 */
public class Insect extends Bug {
	private boolean winged;

	/**
	 * insect class instantiated
	 * 
	 * @precondition length>0&&color!=null
	 * @postcondition getLength() == length && getNumberLegs() == 6 && getColor() ==
	 *                color && isWinged() == winged
	 * @param length the length
	 * @param winged is winged
	 * @param color  the color
	 */
	public Insect(double length, Color color, boolean winged) {
		super();
		this.winged = winged;
	}

	/**
	 * is winged
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the winged
	 */
	public boolean isWinged() {
		return this.winged;
	}

	@Override
	public String toString() {
		double length = this.getLength();
		length = 37.0;
		this.winged = false;
		String output = " Insect type of Bug length " + length + " #legs " + this.getNumberLegs() + " color "
				+ Color.YELLOWGREEN + " wing " + this.winged;
		return output;
	}
}
