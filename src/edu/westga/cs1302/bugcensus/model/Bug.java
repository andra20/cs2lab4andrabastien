package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;
import javafx.scene.paint.Color;

/**
 * The Class Bug.
 * 
 * @author CS1302
 * @version Spring 2020
 */
public class Bug {
	private double length;
	private int numberLegs;
	private Color color;

	/**
	 * instantiate the new Bug class
	 * 
	 * @precondition none
	 * @postcondition length =48.2&& numberLegs = 6 &&Color.GOLDENROD
	 */
	public Bug() {
		this(48.2, 6, Color.GOLDENROD);

	}

	/**
	 * instantiate the new Bug class
	 * 
	 * @precondition length>0&&numberLegs>0 &&color !=null
	 * @postcondition getLength() == length && getNumberLegs() == numberLegs &&
	 *                getColor() == color
	 * @param length     the length
	 * @param numberLegs the number of Legs
	 * @param color      the color
	 * 
	 */
	public Bug(double length, int numberLegs, Color color) {
		if (length < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NONPOSITIVE_LENGTH);
		}
		if (numberLegs < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_NUMBER_LEGS);

		}
		if (color == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_COLOR);
		}
		this.numberLegs = numberLegs;
		this.length = length;
		this.color = color;

	}

	/**
	 * get the length
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the length
	 */
	public double getLength() {
		return this.length;
	}

	/**
	 * get number of legs
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the numberLegs
	 */
	public int getNumberLegs() {
		return this.numberLegs;
	}

	/**
	 * get the color
	 * 
	 * @prcondition none
	 * @postcondition none
	 * @return the color
	 */
	public Color getColor() {
		return this.color;
	}

	@Override
	public String toString() {
		String output = " Bug length " + this.length + " # legs " + this.numberLegs + " color " + this.color;
		return output;

	}

}
