package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;

/**
 * The Enum BugType.
 * 
 * @author CS1302
 * @version Spring 2020
 */
public enum BugType {
	BUG, INSECT, BEE, MYRIAPODA;
	
	/**
	 * Parses the bug type.
	 *
	 * @precondition type != null && BugType.values() contains type
	 * @postcondition none
	 * 
	 * @param type the type to be parsed
	 * @return the bug type as type BugType
	 */
	public static BugType parseType(String type) {
		if (type == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BUGTYPE);
		}
		type = type.toUpperCase();
		switch (type) {
			case "BUG":
				return BUG;
			case "INSECT":
				return INSECT;
			case "BEE":
				return BEE;
			case "MYRIAPODA":
				return MYRIAPODA;
			default:
				throw new IllegalArgumentException("Invalid caste");
		}
	}
}
