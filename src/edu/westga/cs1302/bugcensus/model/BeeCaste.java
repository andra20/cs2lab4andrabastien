package edu.westga.cs1302.bugcensus.model;

import edu.westga.cs1302.bugcensus.resources.UI;

/**
 * the BeeCaste class
 * 
 * @author Andra
 * @version spring 2020
 */
public enum BeeCaste {
	WORKER, DRONE, QUEEN;
	/**
	 * parse Bee Caste in to String
	 * 
	 * @precondition caste !=null
	 * @postcondition none
	 * @param caste the parse
	 * @return the beeCaste as string
	 */
	public static BeeCaste parseCaste(String caste) {
		if (caste == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BEECASTE);

		}
		caste = caste.toUpperCase();
		switch (caste) {
		    case "DRONE":
			    return DRONE;
		    case "WORKER":
			    return WORKER;
		    case "QUEEN":
			    return QUEEN;
	        default:
			    throw new IllegalArgumentException("Invalid caste");

		}
	}
}
