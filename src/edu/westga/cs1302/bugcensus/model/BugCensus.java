/*
 * 
 */
package edu.westga.cs1302.bugcensus.model;

import java.time.LocalDateTime;
import java.util.ArrayList;

import edu.westga.cs1302.bugcensus.resources.UI;

/**
 * The Class BugData.
 * 
 * @author CS1302
 * @version Spring 2020
 */
public class BugCensus {
	private int year;
	private int numberInsect;
	private int numberMyriapodas;
	private ArrayList<Bug> bugs;

	/**
	 * instantiate new BugCensus
	 * 
	 * @precondition none
	 * @postcondition
	 * 
	 */
	public BugCensus() {
		LocalDateTime now = LocalDateTime.now();
		this.year = now.getYear();
		this.bugs = new ArrayList<Bug>();

	}

	/**
	 * Instantiate new BugCensus
	 * 
	 * @precondition year>0
	 * @postcondition
	 * @param year the year
	 */
	public BugCensus(int year) {
		if (year < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NEGATIVE_YEAR);
		}

		this.numberInsect = 0;
		this.numberMyriapodas = 0;
		this.bugs = new ArrayList<Bug>();
		this.year = year;
	}

	/**
	 * get year
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the year
	 */
	public int getYear() {
		return this.year;
	}

	/**
	 * get number of insects
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the numberInsect
	 */
	public int getNumberInsect() {
		return this.numberInsect;
	}

	/**
	 * get myriapoda
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the numberMyriapodas
	 */
	public int getNumberMyriapodas() {
		return this.numberMyriapodas;
	}

	/**
	 * get bugs
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the bugs
	 */
	public ArrayList<Bug> getBugs() {
		return this.bugs;
	}

	/**
	 * get bugs size
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the size of the bugs
	 */
	public int size() {
		return this.bugs.size();

	}

	/**
	 * add bug to list of bugs
	 * 
	 * @precondition bug!=null
	 * @postcondition size()=size prev+1
	 * @param bug the bug
	 * @return true if bug successfully added
	 */

	public boolean add(Bug bug) {
		if (bug == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_BUG);
		}

		return this.add(bug);

	}

	/**
	 * the summary report
	 * 
	 * @precondition none
	 * @postcondition none
	 * @return the summary report for BugCensus
	 */
	public String getSummaryReport() {
		for (Bug currBug : this.bugs) {
			if (currBug instanceof Insect) {
				this.numberInsect++;
			}
			if (currBug instanceof Myriapoda) {

				this.numberMyriapodas++;
			}
		}
		String output = "Bug Census of:" + this.year + System.lineSeparator();
		output += "Total number of bugs:" + this.size() + System.lineSeparator();
		output += "Number of Insects:" + this.numberInsect + System.lineSeparator();
		output += "Total Myriapodas:" + this.numberMyriapodas + System.lineSeparator();
		return output;

	}
}
