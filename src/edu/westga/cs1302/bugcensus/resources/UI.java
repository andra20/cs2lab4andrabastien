package edu.westga.cs1302.bugcensus.resources;

/**
 * The Class UI defines output strings that are displayed for the user to see.
 * 
 * @author CS1302
 * @version Spring 2020
 */
public final class UI {

	/**
	 * Defines string messages for exception messages.
	 */
	public static final class ExceptionMessages {
		public static final String NULL_BUG = "bug cannot be null";	
		public static final String NEGATIVE_YEAR = "year cannot be negative.";
		public static final String NULL_OBSERVATION = "observations cannot be null";
		public static final String NULL_BEECASTE = "bee caste cannot be null";
		public static final String NULL_BUGTYPE = "bug type cannot be null";
		public static final String NONPOSITIVE_LENGTH = "length cannot be less than or equal to 0";
		public static final String NEGATIVE_NUMBER_LEGS = "number legs cannot be negative.";
		public static final String NEGATIVE_NUMBER_SEGMENTS = "number segements cannot be negative.";
		public static final String NULL_COLOR = "color cannot be null";
		
		public static final String NULL_CENSUS_FILE = "census data file cannot be null";
	}

}
