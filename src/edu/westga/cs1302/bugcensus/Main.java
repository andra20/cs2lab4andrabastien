package edu.westga.cs1302.bugcensus;

import edu.westga.cs1302.bugcensus.controller.BugCensusController;
/**
 * Entry point for the program.
 * 
 * @author CS1302
 * @version Spring 2020
 */
public class Main {

	/**
	 * Default entry point for the program.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param args
	 *            command line arguments for the program
	 */
	public static void main(String[] args) {
		BugCensusController censusController = new BugCensusController();
		censusController.generateReports("Census2017.bcs");
	}
}
