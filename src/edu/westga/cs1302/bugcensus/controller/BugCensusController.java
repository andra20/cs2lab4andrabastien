package edu.westga.cs1302.bugcensus.controller;

import java.io.File;
import java.util.ArrayList;
import edu.westga.cs1302.bugcensus.model.Bug;
import edu.westga.cs1302.bugcensus.model.BugCensus;
import edu.westga.cs1302.bugcensus.resources.UI;
import edu.westga.cs1302.bugcensus.datatier.CensusDataReader;

/**
 * The Class BugCensus.
 * 
 * @author CS1302
 *  @version Spring 2020
 */
public class BugCensusController {

	/**
	 * Generates a report for the census stored in the specified file
	 * 
	 * @precondition filename != null && filename is not empty
	 * @postcondition none
	 * 
	 * @param filename the name of the input file with bug data
	 */
	public void generateReports(String filename) {
		BugCensus bugCensus = this.readBugCensusFromFile(filename);
		System.out.println(bugCensus.getSummaryReport());
		System.out.println();

		this.printBugs(bugCensus);
	}

	/**
	 * Reads the bug census from the specified file.
	 * 
	 * @param filename
	 * @return bug census read from file
	 */
	private BugCensus readBugCensusFromFile(String filename) {
		File bugCensusFile = new File(filename);
		CensusDataReader reader = new CensusDataReader(bugCensusFile);
		int year = reader.loadCensusYear();
		ArrayList<Bug> bugs = reader.loadCensusData();
		BugCensus bugCensus = new BugCensus(year);
		bugCensus.getBugs().addAll(bugs);

		return bugCensus;
	}

	/**
	 * Prints bugs in the specified bug census to console
	 * 
	 * @param bugCensus
	 */
	private void printBugs(BugCensus bugCensus) {
		if (bugCensus == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NULL_CENSUS_FILE);
		}
		ArrayList<Bug> bug = bugCensus.getBugs();
		for (Bug currBug : bug) {
			System.out.println(currBug);
		}
	}
}
